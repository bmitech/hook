from os import system, chdir
from os.path import curdir, isdir, isfile, join, abspath, pardir, dirname
from shutil import copy, copyfile
from glob import glob


class BoilerPlate:
    def __init__(self):
        if not self.check_env():
            print('creating .env file')
            if self.clone_env():
                print('.env file created.')
        self.handle_vendor_n_hook()
        if not self.check_node_modules():
            self.install_node_modules()
        print('process completed successfully')

    def handle_vendor_n_hook(self):
        """ Checking Laravel library and copying git hooks """
        if self.check_vendor():
            self.generate_app_key()
            self.clone_hook()
            return True
        else:
            self.make_vendor()
            self.handle_vendor_n_hook()

    def check_env(self):
        """ Check if project has .env file """
        if isfile('.env'):
            return True
        else:
            return False

    def clone_env(self):
        """ Cloning .env.example to .env file. """
        copyfile('.env.example', '.env')
        return True

    def check_vendor(self):
        """ Check if Laravel libraries are installed """
        if isdir('vendor'):
            print('vendor exists.')
            return True
        else:
            print('vendor does not exists.')
            return False

    def make_vendor(self):
        """ Install dependency as per the composer.json file """
        try:
            print('installing dependency')
            system('composer install')
        except Exception:
            print('something went wrong, when installing dependency')
            return False
        else:
            print('dependency installed')
            return True

    def generate_app_key(self):
        """ Generate APP KEY in .env file """
        print('generating app key')
        system('php artisan key:generate')
        return None

    def clone_hook(self):
        """ Cloning files from git-hook to .git/hooks directory """
        for hook in glob(join(abspath(curdir), 'git-hook/*')):
            chdir(join(abspath(join(abspath(curdir), pardir)), '.git/hooks/'))
            copy(hook, abspath(curdir))
        else:
            print('git hooks cloned')
            chdir(dirname(join(curdir, __file__)))

    def check_node_modules(self):
        """ Check if Node Packages are installed """
        if isdir('node_modules'):
            print('node modules exists.')
            return True
        else:
            print('node modules not exists.')
            return False

    def install_node_modules(self):
        """ Perform npm install """
        print('installing node modules')
        system('npm install')

bp = BoilerPlate()