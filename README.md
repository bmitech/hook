# Hook v1.0 #
This repository is based on git hooks. The idea behind this repository is there are basic task, which could be addressed automatically, using small python script. So, that developer doesn't need to engage in simple task, and those get address automatically.

### Git Hooks ###

* Git hooks are scripts that Git executes before or after events such as: commit, push, and receive
* Git hooks are a built-in feature - no need to download anything. Git hooks are run locally

### Project Description ###

* Install Python 3
* I've used generic folder structure and files, which we use in our daily development.
* Run `setup_hook.py` to initiate git hook

### Contribution guidelines ###

* Code review
* Feel free to add elegant enhancements

### Who do I talk to? ###

* Contact **gauravdave@brandmovers.in** in case of any query or improvement.